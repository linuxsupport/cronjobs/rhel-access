# rhel-access

This repository runs the [linuxsoft-rhel-access](https://gitlab.cern.ch/linuxsupport/rpms/linuxsoft-rhel-access) script periodically

Note: as of 16.03.2022, CERN has a site license for RHEL - as such this script only defines .htaccess content for non RHEL products (ELS, EUS, EV)

