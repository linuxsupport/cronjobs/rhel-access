job "${PREFIX}_rhel-access" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_rhel-access" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/rhel-access/rhel-access:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_rhel-access"
        }
      }
      volumes = [
        "/mnt/data1:/mnt/data1",
        "$RHEL_ACCESS_CONFIG:/etc/linuxsoft-rhel-access.conf",
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      ADMIN_EMAIL = "$ADMIN_EMAIL"
      TAG = "${PREFIX}_rhel-access"
    }

    resources {
      cpu = 1000 # Mhz
      memory = 256 # MB
    }

  }
}

